/*
 * Author: Uenal Akkaya
 * Description: Script for the tab capturing chrome extension
 */


//--- Communication [Websocket] ------------------------------------------------------------
//------------------------------------------------------------------------------------------

/*-- start websocket for communication --*/
var wsUri = "ws://127.0.0.1:10981/dlnaserver";
var webSocket;
function startWebSocket() {
	console.info("create Websocket");
    webSocket = new WebSocket(wsUri);
    
	webSocket.onopen = function(e) {
        console.info("opened websocket");
	}
	webSocket.onclose = function(e) {
		setTimeout(function() {startWebSocket()}, 1000);
	}
	webSocket.onmessage = function(e) {
		var msg = JSON.parse(e.data);
		var answer = handleMessage(msg);
        if(answer != null)
		  webSocket.send(answer);
	}
	webSocket.onerror = function(e) {
		//setTimeout(function() {startWebSocket()}, 1000);
	}
}

/*-- decode message and call the requested function --*/
function handleMessage(msg) {
	var f = msg.function;
	switch(f) {
		case "sendURL": (setNewURL(msg.url, msg.cookie));break;
		case "click": (simulateClick(msg.x, msg.y, (msg.key == "left" ? 0: 2)));break;//---
		case "move": (simulateClick(msg.x, msg.y, 1)); break;//---
		case "scroll": (scrollBy(msg.dx, msg.dy));break;
		case "zoom": (zoom(msg.factor));break;
		case "controllVideo": return(controllVideo(msg.key, msg.value));break;
		case "getInformation": return(getInformation());break;
		case "pageBack": return (goBack());break;
		case "pageForward": return (goForward());break;
		case "refreshPage": refreshPage(); break;
	}
}

function controllVideo(control, value) {
	switch(control) {
		case "play": videoPlay();break;
		case "pause": videoPause();break;
		case "stop": videoStop();break;
		case "volume": videoVolume(value);break;
		case "fullscreen": videoFullscreen();break;
	}
	return true;
}

var isVideoPlayable = false;
function getInformation() {
	isVideoPlayable = false;
	
	return JSON.stringify({
		playable_video: videoPlayable(),
		status: videoStatus(),
		loaded: videoLoaded()
	});
}


//--- video controls/informations [HTML5 video tag]-----------------------------------------
//------------------------------------------------------------------------------------------

function videoPlay() {
	var newScript = "$('video').get(0).play()";
	injectJSCode(newScript);
}

function videoPause() {
	var newScript = "$('video').get(0).pause()";
	injectJSCode(newScript);
}

function videoStop() {
	videoPause();
	var newScript = "$('video').get(0).currentTime = 0;";
	injectJSCode(newScript);
}

function videoVolume(v) {
	if(v < 0) v=0;
	if(v > 1) v=1;
	var newScript = "$('video').get(0).volume = "+v+";";
	injectJSCode(newScript);
}

function videoFullscreen() {
	injectJSCode("document.webkitCancelFullScreen();");
}

function videoPlayable() {
	isVideoPlayable = false;
	var newScript = "($('video').get(0).canPlayType('video/webm') === 'probably') || "+
		"($('video').get(0).canPlayType('video/mp4') === 'probably') || "+
		"($('video').get(0).canPlayType('video/ogg') === 'probably') ||"+
		"($('video').get(0).canPlayType('video/webm') === 'maybe') || "+
		"($('video').get(0).canPlayType('video/mp4') === 'maybe') || "+
		"($('video').get(0).canPlayType('video/ogg') === 'maybe')";
	
	chrome.tabs.executeScript(null, {code: newScript}, function(result) {
		isVideoPlayable = result[0];
	});
	
	return isVideoPlayable;
}

function videoStatus() {
	if(!isVideoPlayable) return "null";
	var status = "null";
	var newScript = "var k; \
		if($('video').get(0).ended) k='stop'; \
		else if($('video').get(0).paused) k='pause'; \
		else k='play'; k;";
	
	chrome.tabs.executeScript(null, {code: newScript}, function(result) {
		status = result[0];
	});
	
	return status;
}

function videoLoaded() {
	if(!isVideoPlayable) return false;
	var loaded = false;
	var newScript = "($('video').get(0).readyState === 4)";
	
	chrome.tabs.executeScript(null, {code: newScript}, function(result) {
		loaded = result[0];
	});
	
	return loaded;
}


//--- View manipulation [zoom, scroll, url] ------------------------------------------------
//------------------------------------------------------------------------------------------
var zoomFactor = 100;
function zoom(factor) {
	if(!isInt(factor) || factor < -100 || factor > 100)
		return false;
	
	zoomFactor = zoomFactor+factor;
	if(zoomFactor > 200) zoomFactor = 200;
	if(zoomFactor < 10) zoomFactor = 10;

	injectJSCode("document.body.style.zoom='"+zoomFactor+"%'");
	return true;
}

/*-- scroll horizontal dx pixels and vertical dy pixels  --*/
function scrollBy(dx, dy) {
	if(dx < -1000 || dy < -1000 || dx > 1000 || dy > 1000 || !isInt(dx) || !isInt(dy))
		return false;
	
	var newScript = "window.scrollBy("+dx+","+dy+");";
	injectJSCode(newScript);
	return true;
}

/*-- Load the new Website : update the URL, optional setCookie --*/
function setNewURL(newURL, cookie) {
	if(!/^https?:\/\//i.test(newURL)) newURL = "http://"+newURL;
	
	if(cookie != null) {
		// expiration date of the cookie is current time + one week
		var expDate = ((new Date()).getTime()/1000)+3600*24*7;
		chrome.cookies.set({
			url: newURL,
			expirationDate: expDate
		});
	}
	
	chrome.tabs.update({url: newURL});
	return true;
}

function refreshPage() {
	chrome.tabs.reload(null, null, null);
}

function goBack() {
	injectJSCode("history.back();");
}

function goForward() {
	injectJSCode("history.forward();");
}

//--- Other functions ----------------------------------------------------------------------
//------------------------------------------------------------------------------------------

startWebSocket();

function injectJSCode(jsCode) {
	injectJQuery();
	chrome.tabs.executeScript(null, {code: jsCode}, null);
}

function injectJQuery() {
	chrome.tabs.executeScript(null, {file: "jquery-2.1.1.min.js"}, null);
}

function isInt(n) {
	return (typeof(n) === "number" && n%1 == 0);
}